#include <QCoreApplication>
#include <QFile>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QFile inputfile;
    QFile outputfile;
    QByteArray ba;
    inputfile.setFileName(argv[1]);
    char f[6];
    if(inputfile.open(QIODevice::ReadOnly))
    {
        ba=inputfile.readAll();
        inputfile.close();
        outputfile.setFileName(argv[2]);
        outputfile.open(QIODevice::WriteOnly);
        for(int i=0;i<ba.size();i++)
        {
            if(i%16==0)
                outputfile.write("\n",1);
            QString str;
            str.sprintf("0x%02X, ",(unsigned char)ba.at(i));
            outputfile.write(str.toLatin1().constData(),str.length());
        }
        outputfile.close();
    }
    return a.exec();
}

